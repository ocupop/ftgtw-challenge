/* Copyright (C) 2017 ReFED
 * 
 * http://www.refed.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see
 * http://www.gnu.org/licenses/agpl-3.0.html.

4 scenarios







 */
(function() {
  $.behaviors("#login_challenge", login);
  $.behaviors(".logout_challenge", logout);
  $.behaviors("#update_profile", updateProfile);
  $.behaviors("#challenge_log", updateLog);

  $.behaviors("#signup_form", signup);

  $.behaviors(".edit_log_entry", edit);
  $.behaviors(".save_log_entry", saveLog);

  function login(form) {
    form = $(form);
    var form_id;

    form.ajaxForm({
      dataType: "json",
      beforeSubmit: function(arr, $form, options) {
        // The array of form data takes the following form:
        // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
        // return false to cancel submit
        form_id = $form.attr("id");

        // Set Message
        var notification = { title: "Login", message: "Loading challenge..." };
        showMessage(notification);
      },
      success: function(resp) {
        window.console.log("Success:", form_id);
        if (resp.error) {
          window.console.log("LOGIN: ", resp.error);
          // showMessage({ title: 'Error', message: resp.error });
          var notification = {
            title: "Error",
            message: "Sorry, that email can't be found. Please try again."
          };
          showMessage(notification);
          setTimeout(location.reload.bind(location), 2500);
        } else {
          window.console.log("LOGIN: ", resp);

          // Set cookie for memberEmail
          $.cookie("memberEmail", resp.email_address, { path: "/" });

          // location.reload();
          window.location.href = "/food-waste-log/";
          $("#overlay").hide();
        }
      },
      error: function() {
        window.console.log("LOGIN: ERROR");
        // var notification = { title: 'Error', message: "Sorry we have experienced an unexpected issue. Please try again." };
        // showMessage(notification);
      }
    });
  }

  function logout(button) {
    button = $(button);
    button.on("click", function() {
      if ($.removeCookie("memberEmail", { path: "/" })) {
        window.location.href = "/";
      }
    });
  }

  $("a.logreg").click(function() {
    $("#overlay").hide();
  });

  function signup(form) {
    form = $(form);
    var form_id;

    form.ajaxForm({
      dataType: "json",
      beforeSubmit: function(arr, $form, options) {
        // The array of form data takes the following form:
        // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
        // return false to cancel submit
        form_id = $form.attr("id");

        // Set Message
        var notification = {
          title: "Signup",
          message: "Signing up for the challenge..."
        };
        showMessage(notification);
      },
      success: function(resp) {
        if (resp.error) {
          showMessage({ title: "Signup: Server Error", message: resp.error });
        } else {
          // Set cookie for memberEmail
          $.cookie("memberEmail", resp.email_address, { path: "/" });

          // location.reload();
          window.location.href = "/how-it-works/";
        }
      },
      error: function() {
        var notification = {
          title: "Signup: Internal Error",
          message:
            "Sorry we have experienced an unexpected issue. Please try again."
        };
        showMessage(notification);
      }
    });
  }

  function showMessage(notification) {
    $("#overlay .message_heading").text(notification.title);
    $("#overlay .message_body").text(notification.message);
    $("#overlay #login_challenge").hide();
    $("#overlay").fadeIn(500);
    // setTimeout(function() {
    //   $('#overlay').fadeOut('slow');
    // }, 2000);
  }

  function getProfile() {
    window.console.log("GETTING PROFILE");
    var memberEmail = $.cookie("memberEmail");
    window.console.log(memberEmail);

    if (memberEmail !== undefined) {
      var form = $("#login_challenge");

      form.find("#email_return").val(memberEmail);

      form.ajaxForm({
        dataType: "json",
        beforeSubmit: function(arr, $form, options) {
          // The array of form data takes the following form:
          // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
          // return false to cancel submit
          window.console.log("SUBMIT LOGIN:");
        },
        success: function(resp) {
          if (resp.error) {
            window.console.log("LOGIN: ", resp.error);
            // showMessage({ title: 'Error', message: resp.error });
          } else {
            // Populate form values with response
            showProfile(resp.merge_fields);
            buildLog(resp.merge_fields);
          }
        },
        error: function() {
          window.console.log("LOGIN: ERROR");
          // showMessage({ title: 'Error', message: 'No response...' });
        }
      });

      form.submit();
    } else {
      $("#overlay").fadeIn(500);
    }
  }

  function buildLog(profile) {
    window.console.log("Build Log: ");
    // var log = [];
    // var weeks = $('#app-box').find('fieldset');

    // weeks.each(function(i) {
    //   var w = {};
    //   w.num = $(this).find('input').val();
    //   w.comment = $(this).find('textarea').val();
    //   if(w.num) {
    //     log.push(w);
    //   }
    // });
    // var log_str = JSON.stringify(log);
    // $("#confirm_log").val(log_str);

    // var profile = resp.merge_fields;
    var log = [];

    for (var i = 1; i < 5; i++) {
      var week = {};
      week.num = profile["NUM" + i];
      week.comment = profile["COMMENT" + i];
      if (week.num) {
        log.push(week);
      }
    }
    var log_str = JSON.stringify(log);
    showLog(log_str);
  }
  function showLog(str) {
    var log = [];

    if (str != "") {
      log = JSON.parse(str);
    }

    // Populate form values with response
    var weeks = $("#app-box").find("fieldset");
    for (w = 0; w < log.length; w++) {
      weeks
        .eq(w)
        .find("input")
        .val(log[w].num);
      weeks
        .eq(w)
        .find("textarea")
        .val(log[w].comment);
      weeks.eq(w).addClass("complete");
    }
    if (log.length < weeks.length) {
      weeks.eq(log.length).addClass("current");
    } else {
      // Hide update button and show success...
      // $('#challenge_log input[type="submit"]').hide();
      $("#completion_message").show();
    }
    showStats(log);
  }

  function showStats(log) {
    if (log.length > 0) {
      var week1 = log[0].num / 1;
      var avg = 0;
      var reduction = 0;
      $('[data-stat="week1"] span').text(log[0].num);
      if (log.length == 4) {
        avg = (log[1].num / 1 + log[2].num / 1 + log[3].num / 1) / 3;
        avg = avg.toFixed(2);
        reduction = ((week1 - avg) / week1) * 100;
        reduction = reduction.toFixed(1);
        reductionfb =
          "https://www.facebook.com/dialog/feed?app_id=1759184690986671&link=http://challenge.ivaluefood.com&picture=http://challenge.ivaluefood.com/img/sharenew.jpg&name=I%20Reduced%20my%20Food%20Waste%20by%20" +
          reduction +
          "%%20with%20the%20I%20Value%20Food:%20Too%20Good%20to%20Waste%20Challenge!&display=popup";
        reductiontw =
          "https://twitter.com/home?status=I%20Reduced%20my%20Food%20Waste%20by%20" +
          reduction +
          "%25%20with%20the%20%23IValueFood:%20Too%20Good%20to%20Waste%20Challenge!%20http://challenge.ivaluefood.com/";
      }
      if (avg) {
        $('[data-stat="avg"] span').text(avg);
        $('[data-stat="reduction"] span').text(reduction);
        $('[data-stat="fbshare"]').attr("href", reductionfb);
        $('[data-stat="twshare"]').attr("href", reductiontw);
        $('[data-stat="logshare"]').css("display", "block");
      }
    }
  }

  function showProfile(profile) {
    window.console.log("Show Profile: ", profile);
    var memberEmail = $.cookie("memberEmail");
    // Populate form values with response
    $("#confirm_email").val(memberEmail);
    $("#confirm_first_name").val(profile.FNAME);
    $("#confirm_last_name").val(profile.LNAME);
    $("#confirm_postal").val(profile.POSTAL);
    $("#confirm_organization_code").val(profile.ORGCODE);
  }

  function updateProfile(form) {
    getProfile();

    form = $(form);
    var form_id;

    form.ajaxForm({
      dataType: "json",
      beforeSubmit: function(arr, $form, options) {
        // The array of form data takes the following form:
        // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
        // return false to cancel submit
        form_id = $form.attr("id");
        // Set Message
        showMessage({
          title: "Loading Profile",
          message: "Wait while we load your profile..."
        });
      },
      success: function(resp) {
        if (resp.error) {
          // Set Error Message
          showMessage({ title: "Server Error", message: resp.error });
        } else {
          showMessage({
            title: "Updated Profile",
            message: "Your profile has been successfully updated..."
          });
          showProfile(resp.merge_fields);
        }
      },
      error: function() {
        showMessage({
          title: "Communication Error",
          message:
            "Sorry we have experienced an unexpected issue. Please try again."
        });
      }
    });
  }

  function updateLog(form) {
    getProfile();

    form = $(form);
    var form_id;

    form.ajaxForm({
      dataType: "json",
      beforeSerialize: function($form, options) {
        // return false to cancel submit
        // buildLog();
      },
      beforeSubmit: function(arr, $form, options) {
        // The array of form data takes the following form:
        // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
        // return false to cancel submit
        form_id = $form.attr("id");
        // Set Message
        showMessage({
          title: "Updating Log",
          message: "Wait while we update your log..."
        });
      },
      success: function(resp) {
        if (resp.error) {
          // Set Error Message
          showMessage({ title: "Server Error", message: resp.error });
        } else {
          showMessage({
            title: "Updated Log",
            message: "Your log has been successfully updated..."
          });
          buildLog(resp.merge_fields);
        }
      },
      error: function() {
        showMessage({
          title: "Communication Error",
          message:
            "Sorry we have experienced an unexpected issue. Please try again."
        });
      }
    });
  }

  function saveLog(link) {
    link = $(link);
    form = $("#challenge_log");

    link.on("click", function(e) {
      e.preventDefault();
      form.submit();
    });
  }

  function edit(link) {
    link = $(link);
    link.on("click", function(e) {
      $(this)
        .parent()
        .parent()
        .toggleClass("current complete");
      $(this).toggleClass("hide");
      $(this)
        .parent()
        .parent()
        .find(".save_log_entry")
        .toggleClass("show");
      e.preventDefault();
    });
  }
})();
